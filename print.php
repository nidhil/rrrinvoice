<?php
include('lib/php/sql_functions.php');
include('lib/php/sessionVerify.php');
include('lib/plugins/tfpdf/tfpdf.php');

if(!checkSess()){
    echo 'Invalid session';
    exit;
}
if(isset($_GET['id'])&&!preg_match('/[a-zA-Z|&]/',$_GET['id'])){
    if($rs=getRows('invoices','invoice_id='.$_GET['id'].' AND isDelete=0 ORDER BY invoice_date DESC')){
        while($r=$rs->fetch_assoc()){
            $r['items']=[];
            if($rs_items=getRows('invoice_items','invoice_id='.$r['invoice_id'])){
                while($r_item=$rs_items->fetch_assoc()){
                    $r['items'][]=$r_item;
                }
            }
            $resp=$r;
        }
    }else{
        echo 'Invalid invoice';
        exit;
    }
}else{
    echo 'Invalid URL';
    exit;
}

class InvoicePrint extends tFPDF {

    var $pageWidth=210;
    var $pageHeight=297;
    var $cellHeight=5;
    var $leftMargin=5;
    var $rightMargin=5;

    var $contentStart=82;
    var $contentEnd=240;

    function drawPageLines(){
        $this->SetAutoPageBreak(false);
        $this->AddPage();
        $this->drawMargin();
        // $this->drawTitle();
        // $this->drawTitle2();
        // $this->drawToSection();
        // $this->drawParticulars();
        // $this->drawFooter();
        // $this->addParticulars(1);
    }

    function drawMargin(){
        $this->Line($this->leftMargin,$this->cellHeight,$this->pageWidth-$this->rightMargin,$this->cellHeight);//top
        $this->Line($this->leftMargin,$this->cellHeight,$this->leftMargin,$this->pageHeight-$this->cellHeight);//left
        $this->Line($this->pageWidth-$this->rightMargin,$this->cellHeight,$this->pageWidth-$this->rightMargin,$this->pageHeight-$this->cellHeight);//right
        $this->Line($this->leftMargin,$this->pageHeight-$this->cellHeight,$this->pageWidth-$this->rightMargin,$this->pageHeight-$this->cellHeight);//bottom
    }

    function drawTitle(){
        $content1="Specials in: All kinds of Air-conditioning, Refrigeration, Motor-winding & Electrical Repairs.";
        $content2="Shop No. 7, Satguru Apt., Mohane Road, Shahad (W), Kalyan.";
        $content3="GSTIN : ASADF23124ASD";
        $border_bottom=30;
        $this->setY(2*$this->cellHeight);
        $this->setX($this->leftMargin);
        $this->SetFont('Arial','BI',20);
        $this->MultiCell($this->pageWidth-$this->getMargin(),$this->cellHeight,'RELIANCE REWINDERS & REFRIGERATORS',0,'C');
        $this->SetFont('Arial','B',10);
        $this->setX($this->leftMargin);
        $this->MultiCell($this->pageWidth-$this->getMargin(),$this->cellHeight,$content1,0,'C');
        $this->setX($this->leftMargin);
        $this->MultiCell($this->pageWidth-$this->getMargin(),$this->cellHeight,$content2,0,'C');
        $this->setX($this->leftMargin);
        $this->Cell($this->pageWidth-$this->getMargin(),$this->cellHeight,$content3,0,0,'C');
        $this->setX($this->leftMargin);
        $this->MultiCell($this->pageWidth-$this->getMargin(),$this->cellHeight,'Mob. No. 08898626461',0,'R');
        $this->Line($this->leftMargin,$border_bottom,$this->pageWidth-$this->rightMargin,$border_bottom);
    }
    function drawTitle2($r){
        $this->SetFont('Arial','',10);
        $sep=0.5;
        $cont_start=$this->getY();
        $cont_end=$cont_start+23;
        $this->setY($cont_start+2);
        $this->setX($this->leftMargin+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.3,$this->cellHeight,'Reverse Charge');
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.7,$this->cellHeight,': No. 32121549');
        $this->Ln();
        $this->setX($this->leftMargin+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.3,$this->cellHeight,'GSTIN');
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.7,$this->cellHeight,': ALPSDPKPDS');
        $this->Ln();
        $this->setX($this->leftMargin+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.3,$this->cellHeight,'PAN');
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.7,$this->cellHeight,': asd4645sd');
        $this->Ln();
        $this->setX($this->leftMargin+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.3,$this->cellHeight,'STATE');
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.36,$this->cellHeight,': MAHARASHTRA');
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.12,$this->cellHeight,'CODE');
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep*0.12,$this->cellHeight,'27',1,0,'C');
        
        $this->setY($cont_start+2);
        $this->setX((($this->pageWidth-$this->getMargin())*$sep)+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep)*0.25,$this->cellHeight,'Date');
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep)*0.7,$this->cellHeight,': '.$r['invoice_date']);
        $this->Ln();
        $this->setX((($this->pageWidth-$this->getMargin())*$sep)+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep)*0.25,$this->cellHeight,'Invoice No.');
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep)*0.7,$this->cellHeight,': '.$r['invoice_number']);
        $this->Ln();
        $this->Ln();
        $this->setX((($this->pageWidth-$this->getMargin())*$sep)+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep)*0.25,$this->cellHeight,'Nature of work');
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep)*0.7,$this->cellHeight,': '.$r['nature_of_work']);
        //Lines
        $this->Line(($this->pageWidth-$this->getMargin())*$sep,$cont_start,($this->pageWidth-$this->getMargin())*$sep,$cont_end);
        $this->Line($this->leftMargin,$cont_end,$this->pageWidth-$this->rightMargin,$cont_end);
    }
    function drawToSection($r){
        $sep=0.1;
        $cont_start=52;
        $cont_end=$cont_start+($this->cellHeight*6);
        $this->setY($cont_start+2);
        $this->setX($this->leftMargin+2);
        $this->Cell($this->pageWidth-$this->getMargin(),$this->cellHeight,'Billed To :');
        $this->Ln();
        
        $this->setX($this->leftMargin+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep,$this->cellHeight,'Name');
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep),$this->cellHeight,': M/s '.$r['customer_name']);
        $this->Ln();
        
        $this->setX($this->leftMargin+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep,$this->cellHeight,'Address');
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep),$this->cellHeight,': '.$r['customer_address']);
        $this->Ln();

        $this->setX($this->leftMargin+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep,$this->cellHeight,'State');
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep-0.7),$this->cellHeight,': '.$r['customer_state']);
        $this->Cell(($this->pageWidth-$this->getMargin())*0.6,$this->cellHeight,'Code : '.$r['state_code']);
        $this->Ln();

        $this->setX($this->leftMargin+2);
        $this->Cell(($this->pageWidth-$this->getMargin())*$sep,$this->cellHeight,'GSTIN');
        $this->Cell(($this->pageWidth-$this->getMargin())*(1-$sep),$this->cellHeight,': '.$r['gstin']);
        $this->Ln();

        $sep=0.5;
        $pad=2;

        $hstart=($this->pageWidth-$this->getMargin())*$sep;
        $hend=($this->pageWidth-$this->getMargin());
        $this->Line($hstart,$cont_start+1,$hstart,$cont_end);
        $this->setY($cont_start+2);
        $this->setX($hstart+$pad);
        
        $sep=0.3;
        
        $this->setX($hstart+$pad);
        $this->Cell(($hend-$hstart)*$sep,$this->cellHeight,'Your Order No');
        $this->Cell(($hend-$hstart)*(1-$sep),$this->cellHeight,': '.$r['order_number']);
        $this->Ln();
        $this->setX($hstart+$pad);
        $this->Cell(($hend-$hstart)*$sep,$this->cellHeight,'Your Order Date');
        $this->Cell(($hend-$hstart)*(1-$sep),$this->cellHeight,': '.$r['order_date']);
        $this->Ln();
        $this->setX($hstart+$pad);
        $this->Cell(($hend-$hstart)*$sep,$this->cellHeight,'Other Ref.');
        $this->MultiCell(($hend-$hstart)*(1-$sep),$this->cellHeight,': '.$r['order_ref']);
        $this->Ln();

        //line
        $this->Line($this->leftMargin,$cont_end,$this->pageWidth-$this->rightMargin,$cont_end);
    }

    function drawParticulars(){
        $sect_widths=[0.05,0.53,0.1,0.07,0.1,0.15];
        $cont=$this->pageWidth-$this->getMargin();
        $cont_start=$this->contentStart;
        $cont_end=$this->contentEnd;
        $xa=0;
        $ws=[];
        foreach($sect_widths as $v){
            $xa+=$v;
            
            $x=$this->leftMargin+(($this->pageWidth-$this->rightMargin)*$xa);
            $ws[]=$x;
            $this->Line($x,$cont_start,$x,$cont_end);
        }
        $this->setY($cont_start+1);
        $this->setX($this->leftMargin);
        $this->Cell($sect_widths[0]*$cont,$this->cellHeight,'#',0,0,'C');
        $this->Cell($sect_widths[1]*$cont,$this->cellHeight,'DESCRIPTION OF PRODUCTS',0,0,'C');
        $this->setX($this->getX()+3);
        $this->Cell($sect_widths[2]*$cont,$this->cellHeight,'HSN/SAC',0,0,'C');
        $this->Cell($sect_widths[3]*$cont,$this->cellHeight,'QTY',0,0,'C');
        $this->Cell($sect_widths[4]*$cont,$this->cellHeight,'RATE',0,0,'C');
        $this->setX($this->getX()-1);
        $this->Cell($sect_widths[5]*$cont,$this->cellHeight,'TOTAL',0,0,'C');
        $this->Ln();
        $this->Line($this->leftMargin,$this->getY(),$this->pageWidth-$this->rightMargin,$this->getY());
        $this->Line($this->leftMargin,$cont_end,$this->pageWidth-$this->rightMargin,$cont_end);
    }

    function drawFooter($r){
        $this->setTotal($r);
    }

    function setTotal($r){
        
        $total_before=0;
        $total_after=0;
        
        foreach($r['items'] as $v){
            $total_before+=($v['quantity']*$v['rate']);
        }
        $cgst=$r['cgst']*$total_before/100;
        $sgst=$r['sgst']*$total_before/100;
        $total_after=$total_before+$cgst+$sgst;


        // $this->SetFont('DejaVu','',10);
        $cont_start=$this->contentEnd;
        $cont_end=$this->pageHeight-$this->cellHeight;
        $cont_height=$cont_end-$cont_start;
        $cellHeight=0.15;
        
        $sep=0.58;
        
        $x=$this->leftMargin+(($this->pageWidth-$this->getMargin())*$sep);
        $this->Line($x,$cont_start,$x,$cont_end);

        for($i=0;$i<4;$i++){
            $this->Line($x,$cont_start+$cont_height*$cellHeight*($i+1),$this->pageWidth-$this->rightMargin,$cont_start+$cont_height*$cellHeight*($i+1));
        }

        $x1=179.25;
        $y1=4*$cellHeight*$cont_height;

        $this->setY($cont_start);
        $this->setX($x);
        $this->Cell($x1-$x,$cellHeight*$cont_height,'Total Value Before Tax');
        $this->Cell($this->pageWidth-$this->rightMargin-$x1,$cellHeight*$cont_height,'₹ '.$total_before);
        $this->Ln();
        //for gst seperation
        $x2=$x+($x1-$x)/2;
        $this->Line($x2,$cont_start+($cont_height*$cellHeight),$x2,$cont_start+($cont_height*$cellHeight*3));
        
        $this->setX($x);
        $this->Cell(($x1-$x)*0.5,$cellHeight*$cont_height,'Add : CGST');
        $this->Cell(($x1-$x)*0.5,$cellHeight*$cont_height,$r['cgst'].'%',0,0,'C');
        $this->Cell($this->pageWidth-$this->rightMargin-$x1,0.15*$cont_height,'₹ '.$cgst);
        $this->Ln();
        $this->setX($x);
        $this->Cell(($x1-$x)*0.5,$cellHeight*$cont_height,'Add : SGST');
        $this->Cell(($x1-$x)*0.5,$cellHeight*$cont_height,$r['sgst'].'%',0,0,'C');
        $this->Cell($this->pageWidth-$this->rightMargin-$x1,$cellHeight*$cont_height,'₹ '.$sgst);
        $this->Ln();
        // $this->setX($x);
        // $this->Cell(($x1-$x)*0.5,$cellHeight*$cont_height,'Add : IGST');
        // $this->Cell(($x1-$x)*0.5,$cellHeight*$cont_height,'9%',0,0,'C');
        // $this->Cell($this->pageWidth-$this->rightMargin-$x1,$cellHeight*$cont_height,'₹ 250');
        // $this->Ln();
        $this->setX($x);
        $this->Cell($x1-$x,$cellHeight*$cont_height,'TOTAL BILL VALUE');
        $this->Cell($this->pageWidth-$this->rightMargin-$x1,$cellHeight*$cont_height,'₹ '.$total_after);
        $this->Ln();
        
        $this->setY($this->pageHeight-($this->cellHeight*2));
        $this->setX($x);
        $this->Cell($this->pageWidth-$this->rightMargin-$x,$this->cellHeight,strtoupper('For Reliance Rewinders & Refrigerators'),0,0,'C');
        
        $this->Line($x1,$cont_start,$x1,$cont_start+$y1);

        $hstart=$this->leftMargin;
        $hend=$x;
        $vstart=$cont_start;
        $vend=$cont_start+($cont_height*0.3);
        $this->setY($vstart+1);
        $this->setX($hstart);
        $this->MultiCell($hend-$hstart,$this->cellHeight,'Amount in words : '.getIndianCurrency($total_after).' only');
        $this->Line($hstart,$vend,$hend,$vend);

        $vstart=$vend;
        $vend=$this->pageHeight-$this->cellHeight;
        $this->setY($vstart);
        $this->setX($hstart);
        $this->Cell($hend-$hstart,$this->cellHeight,'Terms & Conditions:');
        $this->Ln();
        $p=2;
        $this->setX($hstart);
        $this->Cell($p,$this->cellHeight,'1.');
        $this->setX($this->getX()+2);
        $this->MultiCell($hend-$hstart-$p-2,$this->cellHeight,'This is to certify that we have a valid registration under GST and above information is true');

        // $this->setX($hstart);
        // $this->Cell($p,$this->cellHeight,'2.');
        // $this->setX($this->getX()+2);
        // $this->MultiCell($hend-$hstart-$p-2,$this->cellHeight,'Subject to Mumbai Jurisdiction');
        
        $this->setX($hstart);
        $this->Cell($p,$this->cellHeight,'2.');
        $this->setX($this->getX()+2);
        $this->MultiCell($hend-$hstart-$p-2,$this->cellHeight,'E. & O.E.');
    }

    function getMargin(){
        return $this->leftMargin+$this->rightMargin;
    }

    function addParticulars($a){
        $sect_widths=[0.05,0.53,0.1,0.07,0.1,0.15];
        $y=$this->contentStart+$this->cellHeight+3;
        $my=0;
        for($i=0;$i<count($a);$i++){
            if($my>$y){
                $y=$my;
            }
            $x=$this->leftMargin;
            $this->setY($y);
            $this->setX($x);
            $this->MultiCell($sect_widths[0]*($this->pageWidth),$this->cellHeight,$i+1,0,'C');

            $x+=$sect_widths[0]*($this->pageWidth);
            $this->setY($y);
            $this->setX($x);
            $this->MultiCell($sect_widths[1]*($this->pageWidth)-3,$this->cellHeight,$a[$i]['name']);
            if($this->getY()>$my){
                $my=$this->getY();
            }

            $x+=$sect_widths[1]*($this->pageWidth)-3;
            $this->setY($y);
            $this->setX($x);
            $this->MultiCell($sect_widths[2]*($this->pageWidth),$this->cellHeight,$a[$i]['hsn'],0,'C');

            $x+=$sect_widths[2]*($this->pageWidth)-1;
            $this->setY($y);
            $this->setX($x);
            $this->MultiCell($sect_widths[3]*($this->pageWidth),$this->cellHeight,$a[$i]['quantity'],0,'C');

            $x+=$sect_widths[3]*($this->pageWidth);
            $this->setY($y);
            $this->setX($x);
            $this->MultiCell($sect_widths[4]*($this->pageWidth),$this->cellHeight,$a[$i]['rate'],0,'C');

            $x+=$sect_widths[4]*($this->pageWidth);
            $this->setY($y);
            $this->setX($x);
            $this->MultiCell($sect_widths[5]*($this->pageWidth-$this->getMargin())-4,$this->cellHeight,$a[$i]['quantity']*$a[$i]['rate'],0,'C');
        }
            

    }
}

$pdf=new InvoicePrint();
// Add a Unicode font (uses UTF-8)
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->drawPageLines();
$pdf->drawTitle();
$pdf->drawTitle2($resp);
$pdf->drawToSection($resp);
$pdf->drawParticulars();
$pdf->drawFooter($resp);
$pdf->addParticulars($resp['items']);

$pdf->Output();
$pdf->Close();


//functions

function getIndianCurrency($number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? '' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? ucfirst($words[$number]).' '. ucfirst($digits[$counter]).' '.ucfirst($hundred):ucfirst($words[floor($number / 10) * 10]).' '.ucfirst($words[$number % 10]). ' '.ucfirst($digits[$counter]).' '.ucfirst($hundred);
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? 'Rupees '.$Rupees : '') . ($paise!=''?$paise .' Paise':'');
}
?>