-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 05, 2017 at 12:11 PM
-- Server version: 5.6.28-76.1-log
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dailymsg_rrr`
--

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `invoice_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(300) DEFAULT NULL,
  `order_number` varchar(100) DEFAULT NULL,
  `order_ref` varchar(100) DEFAULT NULL,
  `invoice_date` varchar(15) DEFAULT NULL,
  `nature_of_work` varchar(20) NOT NULL,
  `customer_address` varchar(1000) NOT NULL,
  `customer_state` varchar(30) NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `gstin` varchar(20) NOT NULL,
  `invoice_number` varchar(50) NOT NULL,
  `order_date` varchar(20) NOT NULL,
  `cgst` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sgst` smallint(5) unsigned NOT NULL DEFAULT '0',
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDelete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invoice_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`invoice_id`, `customer_name`, `order_number`, `order_ref`, `invoice_date`, `nature_of_work`, `customer_address`, `customer_state`, `state_code`, `gstin`, `invoice_number`, `order_date`, `cgst`, `sgst`, `added_on`, `isDelete`) VALUES
(1, 'asdasd', 'asdasd', 'asdsad', '2017-07-26', 'asdad', 'asdasdsa', 'MAHARASHTRA', '27', 'asdasd', '', '2017-07-26', 14, 14, '2017-07-27 05:55:52', 1),
(2, 'sadasd', 'asdas', 'asdsad', '2017-07-26', 'asdsa', 'asdsad', 'MAHARASHTRA', '27', 'asdasd', 'asdsad', '2017-07-26', 0, 0, '2017-07-26 13:43:04', 1),
(3, 'Cetury', '1234', '', '2017-07-27', 'Service', 'Shahad', 'MAHARASHTRA', '27', '27AAAAA', '1001', '2017-07-11', 0, 0, '2017-07-27 11:28:51', 1),
(4, 'Century', '1245', '', '2017-07-27', 'Repair', 'Shahad', 'MAHARASHTRA', '27', 'AAAAASSSSSSZ', '21', '2017-07-19', 9, 10, '2017-07-31 06:14:23', 1),
(5, 'Century Rayon', '132', '', '2017-07-29', 'Service', 'Shahad', 'MAHARASHTRA', '27', 'asdasdf1321fsad', '123', '2017-07-26', 9, 9, '2017-07-31 06:39:35', 1),
(6, 'Century', '1246', 'Rpdp', '2017-07-31', 'Labour', 'Shahad', 'MAHARASHTRA', '27', '27AAACC2659Q1Z6.', '16', '2017-07-25', 9, 9, '2017-07-31 07:02:19', 1),
(7, 'Century Rayon.', '', 'RPDP No.18828/E34', '2017-07-03', 'Labour', 'Shahad,Ulhasnagar', 'MAHARASHTRA', '27', '27AAACC2659Q1Z6', '03/17', '', 9, 9, '2017-07-31 12:25:48', 1),
(8, 'Century Rayon.', '', 'RPDP No.18828/E34', '2017-07-03', 'Labour', 'Shahad,Ulhasnagar', 'MAHARASHTRA', '27', '27AAACC2659Q1Z6', '03/17', '', 9, 9, '2017-07-31 12:25:48', 0),
(9, 'Century Rayon', '', '', '2017-07-19', 'Labour', 'Shahad, Ulhasnagar', 'MAHARASHTRA', '27', '27AAACX', '04/17', '', 0, 0, '2017-07-31 12:30:06', 1),
(10, 'Century Rayon', '', 'RPDP No.18914/E34.', '2017-07-19', 'Labour', 'Shahad, Ulhasnagar', 'MAHARASHTRA', '27', '27AAACC2659Q1Z6', '04/17.', '', 9, 9, '2017-07-31 12:38:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE IF NOT EXISTS `invoice_items` (
  `invoice_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `quantity` int(10) unsigned DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `hsn` varchar(20) NOT NULL,
  KEY `invoice_id` (`invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_items`
--

INSERT INTO `invoice_items` (`invoice_id`, `name`, `quantity`, `rate`, `hsn`) VALUES
(1, 'Itkd djskdk dsksyej ssksks dhsjjd djkskeb djskdh jdjsk jskisdj', 10, 350, 'Hggd'),
(3, 'Cooling coi', 8, 8000, '8234'),
(1, 'HELLO', 10, 850, 'HDSD'),
(1, 'Item 2', 1, 4500, 'Jdkd'),
(1, 'hbh', 10, 500, 'bhbj'),
(1, 'hbh', 10, 500, 'bhbj'),
(4, 'hbh', 10, 500, 'bhbj'),
(5, 'Item 1', 20, 600, 'ABCD'),
(6, 'Labour charges for rewinding of pot motor stator core winding Japan thoshiba make.', 100, 205, ''),
(7, 'Labour charges for rewinding of pot motor stator core winding Japan thoshiba make', 100, 205, 'SAC998719.'),
(8, 'Labour charges for rewinding of pot motor stator core winding Japan thoshiba make', 100, 205, 'SAC998719.'),
(10, 'Labour charges for rewinding of pot motor stator core winding Japan thoshiba make.', 100, 205, 'SAC.998719.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `user_status` varchar(20) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstName`, `username`, `password`, `user_status`, `isAdmin`, `join_date`) VALUES
(1, 'admin', 'admin', 'admin', '1', 1, '2017-07-20 02:43:54');

-- --------------------------------------------------------

--
-- Table structure for table `user_session`
--

CREATE TABLE IF NOT EXISTS `user_session` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_session`
--

INSERT INTO `user_session` (`user_id`, `session_id`) VALUES
(1, 'x2clkjmokfcxcdelhnvt'),
(1, '26gbmdk3iffk3auf69dr'),
(1, 'o07j3mtomsnmy9l6wnkz'),
(1, 'rtcwizvf4i8usris4by7');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
