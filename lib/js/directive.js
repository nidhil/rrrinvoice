angular.module('application')
.directive('datepicker',function(){
    return {
        restrict:'C',
        link:function(scope,elem){
            $(elem).datepicker({
                dateFormat:'yy-mm-dd',
                defaultDate:new Date()
            }).on('change',function(){
                scope.$apply(function(){});
            });
        }
    }
});