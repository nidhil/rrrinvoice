<?php

include ('config.php');

$sql = "CREATE TABLE users (
user_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstName VARCHAR(100),
username VARCHAR(100),
password VARCHAR(200),
user_status VARCHAR(20),
isAdmin BOOLEAN,
join_date TIMESTAMP
)";

if ($con->query($sql) === TRUE) {
    echo "Created table users<br>";
} else {
    echo "Error creating table: " . $con->error;
}

$sql = "CREATE TABLE user_session (
user_id INT UNSIGNED,
session_id VARCHAR(50),
FOREIGN KEY (user_id) REFERENCES users(user_id)
)";

if ($con->query($sql) === TRUE) {
    echo "Created table user_session<br>";
} else {
    echo "Error creating table: " . $con->error;
}

$sql = "CREATE TABLE invoices (
invoice_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
customer_name VARCHAR(300),
order_number VARCHAR(100),
order_ref VARCHAR(100),
invoice_date VARCHAR(15)
)";

if ($con->query($sql) === TRUE) {
    echo "Created table invoices<br>";
} else {
    echo "Error creating table: " . $con->error;
}

$sql = "CREATE TABLE invoice_items (
invoice_id INT UNSIGNED,
item_name VARCHAR(500),
item_qty INT UNSIGNED,
item_rate DOUBLE,
FOREIGN KEY(invoice_id) REFERENCES invoices(invoice_id)
)";

if ($con->query($sql) === TRUE) {
    echo "Created table invoice_items<br>";
} else {
    echo "Error creating table: " . $con->error;
}

?>