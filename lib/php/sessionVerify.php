<?php
include_once('config.php');

function checkSess()
{	global $con;	
	if(isset($_COOKIE['cook_log_usr'])&&isset($_COOKIE['cook_log_sid']))
	{		
		$usr=$_COOKIE['cook_log_usr'];
		$sid=$_COOKIE['cook_log_sid'];
		
		$sql="SELECT * FROM user_session WHERE user_id=".$usr." && session_id='".$sid."';";				
		$res=$con->query($sql);	
		if($res->num_rows>0)
		{
			return true;
		} 
		else 
		{
				clearSession();
				return false;
		}
	}
	else {return false;}
}

function endSession($b)
{
	clearSession();
}

function clearSession()
{
	//setcookie('cook_log_usr', '', time()-3600);
	//setcookie('cook_log_sid', '', time()-3600);
}

function newSession($newuser)
{
	global $con;
	$sid=randStr(20);
	
	$col='user_id';
	$sql="INSERT INTO user_session (".$col.",session_id) VALUES(".$newuser.",'".$sid."')";
	$con->query($sql);	
	setcookie('cook_log_usr', ''.stripslashes($newuser).'',time() + (10 * 365 * 24 * 60 * 60),'/');
	setcookie('cook_log_sid',''.$sid.'',time() + (10 * 365 * 24 * 60 * 60),'/');
	//setcookie('cook_log_type',''.$type.'',time() + (10 * 365 * 24 * 60 * 60),'/');
	//session_start();
	//$_SESSION['new_session']=1;
	return true;
		
}

function delSess()
{	
	global $con;
	if(!(isset($_COOKIE['cook_log_usr'])&&isset($_COOKIE['cook_log_sid'])))
		return true;
	$usr=$_COOKIE['cook_log_usr'];
	$sid=$_COOKIE['cook_log_sid'];
	//$type=$_COOKIE['cook_log_type'];
	//if($type=='admin') $column='admin_id';
	//	else if($type=='user') 
			$column='user_id';
	$sql="DELETE FROM user_session WHERE ".$column."='".$usr."' AND session_id='".$sid."';";
	if($con->query($sql))
	{	setcookie('cook_log_usr', '', time()-3600);
		setcookie('cook_log_sid', '', time()-3600);
		//setcookie('cook_log_type', '', time()-3600);	
		//setcookie('message', '', time()-3600);
		return true;
	
	}
	else return false;
}

function randStr($n)
{	
	$strings=array('0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	$str="";
	for($i=1;$i<=$n;$i++)
	{	
		$str="$str".$strings[rand(0,count($strings)-1)];
	}
	return $str;
}

function getSession(){
	if(checkSess()){
		$info=getSessionInfo();
		unset($info['password']);
		return $info;
	}else{
		return false;
	}
}
function getSessionInfo()
{
	$id=$_COOKIE['cook_log_usr'];

	if(!$rs=getRows('users','user_id='.$id))
	{		
		return false;
	}
	$r=$rs->fetch_assoc();
	return $r;
}
?>