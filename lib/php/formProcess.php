<?php
include('sessionVerify.php');
include('sql_functions.php');

$_ANG=json_decode(file_get_contents('php://input'),true);
if(isset($_ANG)&&count($_ANG)>0){
	$_POST=$_ANG;
	if(isset($_ANG['login'])){
		login();
	}
	if(isset($_ANG['getSession'])){
		getUserSession();
	}
	
}
//anghooks

if(isset($_POST['login']))
login();

function login(){
	$post=$_POST['login'];
	if($rs=getRows('users','username="'.filter_var($post['username'],FILTER_SANITIZE_STRING).'"')){
		$r=$rs->fetch_assoc();
		if($r['password']==$post['pwd']){
			$resp['status']='true';
			newSession($r['user_id']);
		}else{
			$resp['status']='Wrong password';
		}
	}else{
		$resp['status']='Invalid username';
	}
	echo json_encode($resp);
}


function getUserSession(){
	if($info=getSession()){
		$resp['status']='true';
		$resp['info']=$info;
	}else{
		$resp['status']='Invalid session';
	}
	echo json_encode($resp);
}
//endf
?>