<?php
include('../../lib/php/sessionVerify.php');
include('../../lib/php/sql_functions.php');

$_ANG=json_decode(file_get_contents('php://input'),true);
if(isset($_ANG)&&count($_ANG)>0){
	$_POST=$_ANG;
	if(isset($_ANG['getInvoices'])){
		getInvoices();
	}
	if(isset($_ANG['addInvoice'])){
		addInvoice();
	}
	if(isset($_ANG['deleteInvoice'])){
		deleteInvoice();
	}
}
//anghooks

function getInvoices(){
    if(checkSess()){
        $info=getSessionInfo();
    }else{
        $resp['status']='Invalid session';
        echo json_encode($resp);
        return false;
    }
    if($rs=getRows('invoices','isDelete=0 ORDER BY added_on DESC')){
        $resp['status']='true';
        $resp['invoices']=[];
        while($r=$rs->fetch_assoc()){
            $r['items']=[];
            if($rs_items=getRows('invoice_items','invoice_id='.$r['invoice_id'])){
                while($r_item=$rs_items->fetch_assoc()){
			$r_item['name']=str_replace('&#39;','\'',$r_item['name']);
                    $r['items'][]=$r_item;
                }
            }
		$r['invoice_brief']=str_replace('&#39;','\'',$r['invoice_brief']);
            $resp['invoices'][]=$r;
        }
    }else{
        $resp['status']='No invoices';
    }
    echo json_encode($resp);
}
function addInvoice(){
    if(!getSession()){
        $resp['status']='Invalid user';
        echo json_encode($resp);
        return false;
    }
    $post=$_POST['addInvoice'];
    $col['customer_name']=$post['customer_name']; 
    $col['order_number']=$post['order_number']; 
    $col['order_ref']=$post['order_ref']; 
    $col['invoice_date']=$post['invoice_date']; 
    $col['nature_of_work']=$post['nature_of_work']; 
    $col['customer_address']=$post['customer_address']; 
    $col['customer_state']=$post['customer_state']; 
    $col['state_code']=$post['state_code']; 
    $col['gstin']=$post['gstin']; 
    $col['invoice_number']=$post['invoice_number']; 
    $col['cgst']=$post['cgst']; 
    $col['sgst']=$post['sgst']; 
    $col['order_date']=$post['order_date']; 
    $col['gst_type']=$post['gst_type']; 
    $col['invoice_brief']=$post['invoice_brief']; 
    $col['added_on']=date('Y-m-d H:i:s');
    if($id=updateRow('invoices',json_encode($col),'invoice_id='.$post['invoice_id'])){
        $col1['invoice_id']=$post['invoice_id']!='0'?$post['invoice_id']:$id;
        deleteRow('invoice_items','invoice_id='.$post['invoice_id']);
        foreach($post['items'] as $v){
            $col1['name']=$v['name'];
            $col1['quantity']=$v['quantity'];
            $col1['rate']=$v['rate'];
            $col1['hsn']=$v['hsn'];
            insertRow('invoice_items',json_encode($col1));
        }
        $resp['status']='true';
        $resp['invoice_id']=$id;
    }else{
        $resp['status']='Failed to add invoice';
    }
    echo json_encode($resp);

}

function deleteInvoice(){
    if(!getSession()){
        $resp['status']='Invalid user';
        echo json_encode($resp);
        return false;
    }
    $post=$_POST['deleteInvoice'];
    $col['isDelete']=1;
    if(updateRow('invoices',json_encode($col),'invoice_id='.$post['invoice_id'])){
        $resp['status']='true';
    }else{
        $resp['status']='Failed to delete invoice';
    }
    echo json_encode($resp);
}
//endf
?>