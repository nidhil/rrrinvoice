angular.module('application')
.directive('changePasswordModule',function($rootScope){
    return {
        restrict:'E',
        templateUrl:'modules/change-password/change-password.html',
        link:function(scope){
            
            scope.fn={
                changePassword:function(){
                    var par={
                        pwd:scope.var.form.pwd,
                        opwd:scope.var.form.opwd,
                        cpwd:scope.var.form.cpwd
                    };
                    console.log(JSON.stringify(par));
                },

                init:function(){
                    scope.var={
                        form:{
                            opwd:'',
                            pwd:'',
                            cpwd:''
                        }
                    }
                }
            }

            //init
            scope.fn.init();
        }
    }
})